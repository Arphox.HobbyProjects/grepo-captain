﻿;=========================================
;   GENERAL
;=========================================
#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
CoordMode, Mouse, Screen	; MouseGetPos, Click, and MouseMove/Click/Drag Coordinates are relative to the desktop (entire screen).

^Esc::Reload        ; Ctrl+Esc: Reload script
return



;=========================================
;   CSODATÖLTÉS SARCOLÁSSAL (polip nélkül)
;=========================================
;^XButton2::
SendMode Event
Sleep 1000
MsgBox, 0, , A polip legyen kikapcsolva! (5 sec és ez bezáródik), 5
; Sleep 30*60*1000
Loop, 10000
{
	GREPO_BöngészőRefresh()
	GREPO_Sarcolás()
	
	MouseClick, left, 151,118, 1, 6			; "Feljegyzések" megnyitás
	RandomWait(2201, 300)
	
;	; GYORSÍTÁS próba
;		MouseClick, left, 644,457, 1, 6			; 2. sorban lévő szigetre rákattint
;		RandomWait(600, 100)
;		
;		MouseClick, left, 585,462, 1, 6			; "Világcsodainfó" megnyitás
;		RandomWait(3000, 1000)
;		
;		MouseClick, left, 1140,750, 1, 6		; Gyorsítás próba (DIO kikapcsolva)
;		RandomWait(1000, 500)
;		
;		MouseClick, left, 1343,303, 1, 6		; Csoda ablak bezárás
;		RandomWait(500, 500)
	
	MouseClick, left, 644,440, 1, 6			; 1. sorban lévő szigetre rákattint
	RandomWait(601, 100)
	
	MouseClick, left, 585,445, 1, 6			; "Világcsodainfó" megnyitás
	RandomWait(3001, 1000)
	
	Loop, 72
	{
		Send {Right}							; Következő városra vált a jobb nyíl billentyű lenyomásával
		RandomWait(1000, 100)
		
		MouseClick, left, 1244, 768, 1, 1		; DIO "Percentage trade" gombra nyom
		Sleep 100
		
		MouseClick, left, 1140, 766, 1, 1		; "Nyersanyagok küldése" gomb-bal elküldi a nyersit a csodába
		RandomWait(1200, 200)
	}
	
	LoopIdő := 72 * (1000 + 100 + 1200)				; A fenti Loop-ban eltöltött összidő
	CsodaAblakMegnyitásIdő := 2201 + 601 + 3001		; Feljegyzések, 1. sor katt, Világcsodainfó megnyit
	;GyorsításPluszIdő := 600 + 3000 + 1000 + 500	; Csoda gyorsítás próbálásával eltöltött plusz idő
	GyorsításPluszIdő := 0
	RefreshIdő := 9001								; A böngésző refresh után kivárt idő
	SarcElőttiIdő := 6001 + 2001					; A sarc begyűjtés előtti navigációs idő
	
	TeljesLevonás := LoopIdő + CsodaAblakMegnyitásIdő + GyorsításPluszIdő + RefreshIdő + SarcElőttiIdő
	
	RandomWait(600000 - TeljesLevonás, 10000)
}
return


;=========================================
;   CSODATÖLTÉS - 1 kör
;=========================================
!XButton2::
SendMode Event
Sleep 1000
GREPO_FesztiválÉsSzínházPróba()
return



;=========================================
;   10 percenként sarcolás
;=========================================
^XButton2::
SendMode Event
MsgBox, 0, , Mód: 10 percenkénti sarcolás. (5 sec és ez bezáródik), 5
 ;Sleep 66 * 60 * 1000
Loop, 10000
{
										;Sleep 5*60*1000	; + 5 perc várakozás!!!!!!!!
	MsgBox, 0, , Sarcolás! (5 sec és ez bezáródik), 5
	GREPO_BöngészőRefresh()
	GREPO_Belépés_Carphi()
	GREPO_FesztiválPróba()
	MouseClick, left, 283, 146, 1, 6		; Sarcolás 1: Falvak gomb megnyomása
	RandomWait(3000, 200)
	MouseClick, left, 815, 345, 1, 6		; Sarcolás 2: "Mindent kiválaszt" bepipálása
	RandomWait(4000, 200)
	MouseClick, left, 1235, 812, 1, 6		; Sarcolás 3: "Begyűjtés" gombra kattintás
	RandomWait(8000, 400)
	MouseClick, left, 1343, 304, 1, 6		; Sarcolás 4: Sarcolás ablak bezárás
	RandomWait((1200-25) * 1000, 5000)
}
return





;=========================================
;	GREPOLIS Function library
;=========================================
GREPO_Sarcolás()
{
	MouseClick, left, 283, 146, 1, 6		; Sarcolás 1: Falvak gomb megnyomása
	RandomWait(6001, 2000)
	MouseClick, left, 815, 345, 1, 6		; Sarcolás 2: "Mindent kiválaszt" bepipálása
	RandomWait(2001, 1000)
	MouseClick, left, 1235, 812, 1, 6		; Sarcolás 3: "Begyűjtés" gombra kattintás
	RandomWait(10001, 2000)
	MouseClick, left, 1343, 304, 1, 6		; Sarcolás 4: Sarcolás ablak bezárás
	RandomWait(1001, 400)
}
GREPO_BöngészőRefresh()
{
	MouseClick, left, 85, 52, 1, 6			; Böngészőtab refresh
	RandomWait(9001, 1000)
}
GREPO_FesztiválPróba()
{
	MouseMove, 208, 118, 6					; 3. menüpontos gombra egér rávisz
	RandomWait(500, 500)
	MouseClick, left, 237, 286, 1, 6		; Menüponton belül klikk a Kultúra gombra
	RandomWait(1200, 1000)
	MouseClick, left, 1240, 818, 1, 6		; Megkezdés mindenhol PIPA
	RandomWait(2000, 2000)
	MouseClick, left, 1335, 305, 1, 6		; Kultúra áttekintő ablak bezárás
	RandomWait(1001, 400)
}
GREPO_FesztiválÉsSzínházPróba()
{
	MouseMove, 208, 118, 6					; 3. menüpontos gombra egér rávisz
	RandomWait(500, 500)
	MouseClick, left, 237, 286, 1, 6		; Menüponton belül klikk a Kultúra gombra
	RandomWait(1200, 1000)
	MouseClick, left, 1240, 818, 1, 6		; Megkezdés mindenhol PIPA
	RandomWait(2000, 2000)
	MouseClick, left, 1215, 816, 1, 6		; Típuskiválasztó legördülő katt
	RandomWait(500, 500)
	MouseClick, left, 1144, 881, 1, 6		; Színház típus kiválasztás
	RandomWait(500, 500)
	MouseClick, left, 1240, 818, 1, 6		; Megkezdés mindenhol PIPA
	RandomWait(2000, 2000)
	MouseClick, left, 1335, 305, 1, 6		; Kultúra áttekintő ablak bezárás
	RandomWait(1001, 400)
}
GREPO_FesztiválÉsSzínházÉsDiadalPróba()
{
	MouseMove, 208, 118, 6					; 3. menüpontos gombra egér rávisz
	RandomWait(500, 500)
	MouseClick, left, 237, 286, 1, 6		; Menüponton belül klikk a Kultúra gombra
	RandomWait(1200, 1000)
	MouseClick, left, 1240, 818, 1, 6		; Megkezdés mindenhol PIPA (városi fesztivál)
	RandomWait(2000, 2000)
	MouseClick, left, 1215, 816, 1, 6		; Típuskiválasztó legördülő katt
	RandomWait(500, 500)
	MouseClick, left, 1144, 881, 1, 6		; Színház típus kiválasztás
	RandomWait(500, 500)
	MouseClick, left, 1240, 818, 1, 6		; Megkezdés mindenhol PIPA
	RandomWait(2000, 2000)
	MouseClick, left, 1215, 816, 1, 6		; Típuskiválasztó legördülő katt
	RandomWait(500, 500)
	MouseClick, left, 1144, 866, 1, 6		; Diadalmenet típus kiválasztás
	RandomWait(500, 500)
	MouseClick, left, 1240, 818, 1, 6		; Megkezdés mindenhol PIPA
	RandomWait(3000, 2000)
	MouseClick, left, 1335, 305, 1, 6		; Kultúra áttekintő ablak bezárás
	RandomWait(1001, 400)
}
GREPO_Belépés_Carphi()
{
	MouseClick, left, 1095, 720, 1, 6
	RandomWait(10000, 3000)
}




;=========================================
;	Function library
;=========================================
RandomizeCoordinateValue(value)
{
    Random, rnd, -2, 2
	return value+rnd
}
RandomWait(waitAmount, maxDelay)
{
	Random, rndDelay, 0, maxDelay
	Sleep waitAmount+rndDelay
}





;=========================================
;	Hotkey/Fn cheat sheet
;=========================================
; Launch_Media::
; Launch_Mail::
; Launch_App1::
; Launch_App2::
; ^XButton2::			Ctrl + egér előre gomb (Browser_Forward)
; ^XButton1::			Ctrl + egér hátra gomb (Browser_Back)
; Módosítók: # = Win, ^ = Ctrl, ! = Alt, + = Shift
; Loop, 100 { ... }
; SoundPlay, %A_WinDir%\Media\Windows Notify Messaging.wav			; c:\Windows\Media\














;=========================================
;   GREPOLIS UTILS
;=========================================


;=========================================
;   Csere a faluval - Ctrl+MButton
;=========================================
^MButton::
	MouseGetPos, StartX, StartY 
	Sleep 40
	Send {LButton}
	Sleep 40
	Send {Click 1037 608}
	Sleep 40
	Send {Click 944 808}
	Sleep 40
	Send {Click 1310 315}
	MouseMove, StartX, StartY
return


;=========================================
;   Gyors sarcolás - Launch_App1 (jobb felső sarok)
;=========================================
Launch_App1::
	Send {Click 283 146}
	Sleep 350
	Send {Click 815 345}
	Sleep 350
	Send {Click 1235 812}
	Sleep 600
	Send {Click 1343 304}
return