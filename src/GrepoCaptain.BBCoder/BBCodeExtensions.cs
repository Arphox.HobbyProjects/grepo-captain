﻿namespace GrepoCaptain.BBCoder;

public static class BBCodeExtensions
{
    public static BBCode Bold(this BBCode code)
    {
        return code.WrapWithTag("b");
    }

    public static BBCode Italicize(this BBCode code)
    {
        return code.WrapWithTag("i");
    }

    public static BBCode Underline(this BBCode code)
    {
        return code.WrapWithTag("u");
    }

    public static BBCode Strikethrough(this BBCode code)
    {
        return code.WrapWithTag("s");
    }

    public static BBCode Url(this BBCode code)
    {
        return code.WrapWithTag("url");
    }

    public static BBCode Url(this BBCode code, string title)
    {
        return $"[url={code}]{title}[/url] ";
    }

    public static BBCode Img(this BBCode code)
    {
        return code.WrapWithTag("img");
    }

    public static BBCode Quote(this BBCode code)
    {
        return code.WrapWithTag("quote");
    }

    public static BBCode Quote(this BBCode code, string author)
    {
        return $"[quote=\"{author}\"]{code}[/quote]";
    }

    public static BBCode Spoiler(this BBCode code)
    {
        return code.WrapWithTag("spoiler");
    }

    public static BBCode Spoiler(this BBCode code, string title)
    {
        return $"[spoiler={title}]{code}[/spoiler] ";
    }

    internal static BBCode WrapWithTag(this BBCode code, string tag)
    {
        return $"[{tag}]{code}[/{tag}]";
    }
}
