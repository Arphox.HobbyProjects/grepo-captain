namespace GrepoCaptain.BBCoder;

/// <remarks>
/// This simple wrapper object has been created to provide a lightweight layer over
/// <see cref="string"/> to avoid polluting the <see cref="string"/> type with extension methods.
/// </remarks>
public sealed record BBCode
{
    private readonly string _value;

    private BBCode(string value)
    {
        _value = value;
    }

    public static implicit operator BBCode(string value)
    {
        return new BBCode(value);
    }

    public static implicit operator string(BBCode bbcode)
    {
        return bbcode._value;
    }

    public override string ToString()
    {
        return _value;
    }
}
