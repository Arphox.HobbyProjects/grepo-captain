namespace GrepoCaptain.Common.Game;

public interface IHasBbCode
{
    string ToBbCode();
}
