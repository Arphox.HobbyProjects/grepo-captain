using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace GrepoCaptain.Common;

public class ThreadingHelper
{
    /// <summary>
    /// Similar to Task.Run(), except this creates a task that runs on a thread
    /// in an STA apartment rather than Task's MTA apartment.
    /// </summary>
    /// <param name="action">The work to execute asynchronously.</param>
    /// <returns>A task object that represents the work queued to execute on an STA thread.</returns>
    public static Task RunInSTA([NotNull] Action action)
    {
        TaskCompletionSource<object> tcs = new(); // Return type is irrelevant for an Action.

        Thread thread = new(() =>
        {
            try
            {
                action();
                tcs.SetResult(null); // Irrelevant.
            }

            catch (Exception e)
            {
                tcs.SetException(e);
            }
        });

        thread.SetApartmentState(ApartmentState.STA);
        thread.Start();

        return tcs.Task;
    }

    public static string RunInSTASync([NotNull] Func<string> func)
    {
        string returnValue = "";
        Thread thread = new(
            delegate()
            {
                returnValue = func();
            });
        thread.SetApartmentState(ApartmentState.STA);
        thread.Start();
        thread.Join();

        return returnValue;
    }
}
