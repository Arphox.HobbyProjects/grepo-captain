using FluentAssertions;
using GrepoCaptain.BBCoder;
using Xunit;

namespace GrepoCaptain.UnitTest.BBCoder;

public sealed class BBCodeExtensionsTests
{
    [Fact]
    public void Bold()
    {
        // Arrange
        BBCode input = "hello";

        // Act
        string result = input.Bold();

        // Assert
        result.Should().Be("[b]hello[/b]");
    }

    [Fact]
    public void Italicize()
    {
        // Arrange
        BBCode input = "hello";

        // Act
        string result = input.Italicize();

        // Assert
        result.Should().Be("[i]hello[/i]");
    }

    [Fact]
    public void Underline()
    {
        // Arrange
        BBCode input = "hello";

        // Act
        string result = input.Underline();

        // Assert
        result.Should().Be("[u]hello[/u]");
    }

    [Fact]
    public void Strikethrough()
    {
        // Arrange
        BBCode input = "hello";

        // Act
        string result = input.Strikethrough();

        // Assert
        result.Should().Be("[s]hello[/s]");
    }

    [Fact]
    public void Url()
    {
        // Arrange
        BBCode input = @"https://en.wikipedia.org";

        // Act
        string result = input.Url();

        // Assert
        result.Should().Be("[url]https://en.wikipedia.org[/url]");
    }

    [Fact]
    public void UrlWithTitle()
    {
        // Arrange
        BBCode input = @"https://en.wikipedia.org";

        // Act
        string result = input.Url("English Wikipedia");

        // Assert
        result.Should().Be("[url=https://en.wikipedia.org]English Wikipedia[/url] ");
    }

    [Fact]
    public void Img()
    {
        // Arrange
        BBCode input = @"https://upload.wikimedia.org/wikipedia/commons/7/70/Example.png";

        // Act
        string result = input.Img();

        // Assert
        result.Should().Be("[img]https://upload.wikimedia.org/wikipedia/commons/7/70/Example.png[/img]");
    }

    [Fact]
    public void Quote()
    {
        // Arrange
        BBCode input = @"quoted text";

        // Act
        string result = input.Quote();

        // Assert
        result.Should().Be("[quote]quoted text[/quote]");
    }

    [Fact]
    public void QuoteWithAuthor()
    {
        // Arrange
        BBCode input = @"quoted text";

        // Act
        string result = input.Quote("joe");

        // Assert
        result.Should().Be("[quote=\"joe\"]quoted text[/quote]");
    }

    [Fact]
    public void Spoiler()
    {
        // Arrange
        BBCode input = @"secret";

        // Act
        string result = input.Spoiler();

        // Assert
        result.Should().Be("[spoiler]secret[/spoiler]");
    }

    [Fact]
    public void SpoilerWithTitle()
    {
        // Arrange
        BBCode input = @"secret";

        // Act
        string result = input.Spoiler("Very");

        // Assert
        result.Should().Be("[spoiler=Very]secret[/spoiler] ");
    }
}
