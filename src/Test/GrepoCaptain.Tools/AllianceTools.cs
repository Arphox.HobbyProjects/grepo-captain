using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrepoCaptain.InnoDataProvider.Model;
using Xunit;
using static GrepoCaptain.Tools.Utils.ClipboardHelper;

namespace GrepoCaptain.Tools;

public class AllianceTools : ToolBase
{
    private const string AllianceName = "meik kostol be";

    /// <summary>
    /// For all players of the selected alliance, reports for each player
    /// how many farm islands do they have towns on.
    /// Format: each line consisting: PlayerName - FarmIslandCount
    /// Sorted by the number of farm islands, descending.
    /// </summary>
    [Fact]
    public async Task GetForPlayers_HowManyFarmIslandAreTheyOn()
    {
        Alliance alliance = GetAllianceByName(AllianceName);

        var playerStats =
            GetAllianceMembers(alliance)
                .Select(p => new
                {
                    Player = p,
                    FarmIslandCount = GetPlayerTowns(p)
                        .Select(t => t.IslandDesignator)
                        .Distinct()
                        .Select(GetIslandByDesignator)
                        .Count(i => i.IsInhabited),
                })
                .OrderByDescending(p => p.FarmIslandCount)
                .ToArray();

        List<string> export = playerStats
            .Select(x => $"{x.Player.Name} - {x.FarmIslandCount}")
            .ToList();

        export.Add($"Total: {playerStats.Sum(x => x.FarmIslandCount)}");

        await SetClipboard(export);
    }

    /// <summary>
    /// Lists all "pure" islands for an alliance.
    /// "Pure" here means that on that given island, all towns are owned by the given alliance.
    /// </summary>
    [Fact]
    public async Task ForAlliance_ListPureIslands()
    {
        const bool INHABITATED_ONLY = true;
        Alliance alliance = GetAllianceByName(AllianceName);

        IEnumerable<Island> islands = GetPureAllianceIslands(alliance);
        if (INHABITATED_ONLY)
            islands = islands.Where(i => i.IsInhabited);

        IEnumerable<string> exportLines = islands.Select(x => $"{x.ToBbCode()}");
        await SetClipboard(exportLines);
    }

    /// <summary>
    /// For every member of an alliance,
    /// make an estimate of the member's favor production.
    /// </summary>
    [Fact]
    public async Task ForAlliance_EstimateFavorProduction()
    {
        Alliance alliance = GetAllianceByName(AllianceName);
        const bool highPriestess = false;

        var playerStats = GetAllianceMembers(alliance)
            .OrderByDescending(p => p.NumberOfTowns)
            .Select(p => new
            {
                Player = p,
                FavorProductionEstimate = EstimateFavorProductionForTownCount(p.NumberOfTowns, highPriestess),
            })
            .ToArray();

        List<string> export = playerStats
            .Select(x => $"{x.Player.Name}\t{x.FavorProductionEstimate}")
            .ToList();

        export.Add($"TOTAL: {playerStats.Sum(x => x.FavorProductionEstimate)}");

        await SetClipboard(export);
    }

    /// <summary>
    /// Lists the number of pure islands for the top N alliances
    /// "Pure" here means that on that given island, all towns are owned by the given alliance.
    /// </summary>
    [Fact]
    public async Task ForTopNAlliances_ListCountOfPureIslands()
    {
        const bool INHABITATED_ONLY = false;
        const int N = 20; // The top "how many" alliances do you want?

        IEnumerable<Alliance> topAlliances = Alliances
            .OrderByDescending(a => a.Points)
            .Take(N);

        var stat = topAlliances
            .Select(a => new
            {
                Alliance = a,
                Islands = GetIslands(a),
            })
            .ToArray();

        string[] exportLines = stat
            .Select(x => $"{x.Alliance.Name} - {x.Islands.Length}")
            .ToArray();

        await SetClipboard(exportLines);

        // --------------
        Island[] GetIslands(Alliance a)
        {
            IEnumerable<Island> islands = GetPureAllianceIslands(a);
            if (INHABITATED_ONLY)
                islands = islands.Where(i => i.IsInhabited);
            return islands.ToArray();
        }
    }

    /// <summary>
    /// For the top N alliances,
    /// for one alliance:
    /// sums for each player how many farm islands can they farm
    /// </summary>
    [Fact]
    public async Task ForTopNAlliances_ListSumOfPlayersHowManyFarmIslandAreTheyOn()
    {
        const int N = 6; // The top "how many" alliances do you want?

        IEnumerable<Alliance> topAlliances = Alliances
            .OrderByDescending(a => a.Points)
            .Take(N);

        var stat = topAlliances
            .Select(a =>
            {
                var playerStats =
                    GetAllianceMembers(a)
                        .Select(p => new
                        {
                            Player = p,
                            FarmIslandCount = GetPlayerTowns(p)
                                .Select(t => t.IslandDesignator)
                                .Distinct()
                                .Select(GetIslandByDesignator)
                                .Count(i => i.IsInhabited),
                        })
                        .ToArray();

                return new
                {
                    Alliance = a,
                    Count = playerStats.Sum(x => x.FarmIslandCount),
                };
            })
            .ToArray();

        string[] exportLines = stat
            .Select(x => $"{x.Alliance.Name} - {x.Count}")
            .ToArray();

        await SetClipboard(exportLines);
    }

    private IEnumerable<Island> GetPureAllianceIslands(Alliance alliance)
    {
        var islandsWithTowns = GetAllianceIslands(alliance)
            .Select(isl => new
            {
                Island = isl,
                Towns = GetIslandTowns(isl),
            });

        IEnumerable<Island> pureAllianceIslands = islandsWithTowns
            .Where(x => x.Towns.All(IsTownOwnedByAlliance))
            .Select(x => x.Island);

        return pureAllianceIslands;

        // ------------------------------------
        bool IsTownOwnedByAlliance(Town town)
        {
            return !town.IsGhostTown &&
                   GetPlayer(town.PlayerId).AllianceId == alliance.Id;
        }
    }
}
