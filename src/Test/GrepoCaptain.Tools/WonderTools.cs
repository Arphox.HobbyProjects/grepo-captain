using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GrepoCaptain.InnoDataProvider.Model;
using Xunit;
using static GrepoCaptain.Tools.Utils.ClipboardHelper;

namespace GrepoCaptain.Tools;

public sealed class WonderTools : ToolBase
{
    private const string AllianceName = "meik kostol be";

    private readonly Island[] _wonderIslands;

    public WonderTools()
    {
        _wonderIslands = new[]
        {
            GetIsland(57642),
            GetIsland(57631),
            GetIsland(55321),
            GetIsland(55295),
            GetIsland(55311),
            GetIsland(55337),
            GetIsland(57680),
        };
    }

    /// <summary>
    /// Tells for each player:
    /// For each wonder island, how far are the player's towns in total to that wonder island.
    /// These value pairs are ordered by the distance.
    /// </summary>
    [Fact]
    public async Task Calculate_WonderIslandRating_ForEachPlayer_ByDistanceOfAllTownsToWonderIsland()
    {
        Alliance alliance = GetAllianceByName(AllianceName);

        var playersWithTowns = GetAllianceMembers(alliance)
            .Select(p => new
            {
                Player = p,
                Towns = GetPlayerTowns(p),
            })
            .ToArray();

        var playerRanks = playersWithTowns
            .Select(pwt => new
            {
                Player = pwt.Player,
                Towns = pwt.Towns,
                Toplist = _wonderIslands
                    .Select(isl => new
                    {
                        Island = isl,
                        SumOfDistanceFromTownsToIsland = pwt.Towns.Sum(t => t.IslandDistanceTo(isl)),
                    })
                    .OrderBy(x => x.SumOfDistanceFromTownsToIsland)
                    .ToArray(),
            })
            .ToArray();

        string[] exportLines = playerRanks
            .Select(x =>
            {
                string player = x.Player.ToBbCode();
                string islands = string.Join(
                    ", ",
                    x.Toplist.Select(t =>
                        $"{t.Island.ToBbCode()} ({Math.Round(t.SumOfDistanceFromTownsToIsland, 0)})"));

                return $"{player}: {islands}";
            })
            .ToArray();

        await SetClipboard(exportLines);
    }

    [Fact]
    public async void List_ForEachPlayer_HowManyWonderIslandTowns_DoTheyHave()
    {
        Alliance alliance = GetAllianceByName(AllianceName);
        Player[] members = GetAllianceMembers(alliance);

        var playersWithTowns = members
            .Select(p => new
            {
                Player = p,
                Towns = GetPlayerTowns(p),
            })
            .ToArray();

        IEnumerable<string> export = playersWithTowns
            .Select(pwt => new
            {
                pwt.Player,
                WonderIslandTownCount = pwt.Towns.Count(t =>
                    _wonderIslands.Contains(GetIslandByDesignator(t.IslandDesignator))),
            })
            .Where(x => x.WonderIslandTownCount > 0)
            .OrderByDescending(x => x.WonderIslandTownCount)
            .Select(x => $"{x.Player.Name} - {x.WonderIslandTownCount}");

        await SetClipboard(export);
    }

    [Fact]
    public async void Get_all_towns_on_wonder_islands()
    {
        Town[] wonderTowns = _wonderIslands
            .SelectMany(GetIslandTowns)
            .ToArray();

        var townsWithPlayers = wonderTowns
            .Select(t => new
            {
                Town = t,
                Player = GetPlayer(t.PlayerId),
            })
            .OrderBy(t => t.Player.Name)
            .ThenBy(t => t.Town.TownId);

        IEnumerable<string> export = townsWithPlayers
            .Select(x => $"{x.Player.Name}\t{x.Town.Points}");

        await SetClipboard(export);
    }
}
