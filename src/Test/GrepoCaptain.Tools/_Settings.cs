using HuWorlds = GrepoCaptain.InnoDataProvider.Configuration.Constants.HungarianWorldIdentifiers;

namespace GrepoCaptain.Tools;

internal static class Settings
{
    internal const string SelectedWorld = HuWorlds.Carphi;
    internal const int WorldSpeed = 4;
}
