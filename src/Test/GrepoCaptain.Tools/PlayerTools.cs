﻿using System.Linq;
using System.Threading.Tasks;
using GrepoCaptain.InnoDataProvider.Model;
using MoreLinq.Extensions;
using Xunit;
using static GrepoCaptain.Tools.Utils.ClipboardHelper;

namespace GrepoCaptain.Tools;

public sealed class PlayerTools : ToolBase
{
    /// <summary>
    /// For the selected player, reports all towns of the player.
    /// Format: Multiline, one line: BB code of the town.
    /// </summary>
    [Theory]
    [InlineData("Arphox")]
    public async Task GetTowns(string playerName)
    {
        Town[] playerTowns = GetPlayerTowns(GetPlayerExact(playerName))
            .OrderBy(t => t.Name)
            .ToArray();

        var export = playerTowns.Select(t => t.ToBbCode());
        await SetClipboard(export);
    }

    /// <summary>
    /// For the selected player, reports the player's towns in random order.
    /// Format: Multiline, one line: BB code of the town.
    /// </summary>
    [Theory]
    [InlineData("Arphox")]
    public async Task GetTownsInRandomOrder(string playerName)
    {
        Player player = Players.Single(p => p.Name == playerName);
        Town[] playerTownsShuffled = Towns
            .Where(t => t.PlayerId == player.PlayerId)
            .Shuffle()
            .ToArray();

        await SetClipboard(playerTownsShuffled.Select(t => t.ToBbCode()));
    }

    /// <summary>
    /// Select a player.
    /// Select a town by id.
    /// Lists the player's all towns, ordered by
    /// the (island) distance from the selected town.
    /// Format: Multiline, one line: town as BB code
    /// </summary>
    [Theory]
    [InlineData("Arphox", 544)]
    public async Task GetAllTownsOfPlayerByDistanceFromGivenTown(string playerName, int selectedTownId)
    {
        Town referenceTown = Towns.Single(t => t.TownId == selectedTownId);

        var playerTowns = GetPlayerTowns(GetPlayerExact(playerName))
            .Select(t => new
            {
                Town = t,
                Distance = t.IslandDistanceTo(referenceTown),
            })
            .OrderBy(x => x.Distance)
            .ToArray();

        string[] export = playerTowns.Select(x => $"{x.Town.ToBbCode()} - {x.Distance}").ToArray();
        await SetClipboard(export);
    }
}
