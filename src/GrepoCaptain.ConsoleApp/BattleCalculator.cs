using System;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace GrepoCaptain.ConsoleApp;

public static class BattleCalculator
{
    public static void Run()
    {
        int[] biremeCounts =
        {
            // [0-1000] +100
            300,
            400,
            500,
            600,
            700,
            800,
            900,
            1000,
            // [1000-5000] +200
            1200,
            1400,
            1600,
            1800,
            2000,
            2200,
            2400,
            2600,
            2800,
            3000,
            3200,
            3400,
            3600,
            3800,
            4000,
            4200,
            4400,
            4600,
            4800,
            5000,
            // [5k-10k] +500
            5500,
            6000,
            6500,
            7000,
            7500,
            8000,
            8500,
            9000,
            9500,
            // [10k-30k] +1000
            10000,
            11000,
            12000,
            13000,
            14000,
            15000,
            16000,
            17000,
            18000,
            19000,
            20000,
            21000,
            22000,
            23000,
            24000,
            25000,
            26000,
            27000,
            28000,
            29000,
            // [30k-50k] +2000
            30000,
            32000,
            34000,
            36000,
            38000,
            40000,
            42000,
            44000,
            46000,
            48000,
            // [50k-100k] +5000
            50000,
            55000,
            60000,
            65000,
            70000,
            75000,
            80000,
            85000,
            90000,
            95000,
            100000,
        };

        int[] lightShipCounts =
        {
            290,
            300,
            310,
            320,
            330,
            340,
            350,
            360,
            370,
            380,
            390,
            400,
            410,
            420,
            430,
            440,
            450,
        };

        int[,] matrix = new int[biremeCounts.Length, lightShipCounts.Length];

        for (int j = 0; j < lightShipCounts.Length; j++)
        {
            int lightShipCountPerAttack = lightShipCounts[j];
            for (int i = 0; i < biremeCounts.Length; i++)
            {
                int biremeCount = biremeCounts[i];
                int attackCount = CalculateHowManyLightShipAttacksNeeded(lightShipCountPerAttack, biremeCount);
                matrix[i, j] = attackCount;
            }
        }

        ExportToClipboard(biremeCounts, lightShipCounts, matrix);
    }

    private static void ExportToClipboard(int[] biremeCounts, int[] lightShipCounts, int[,] matrix)
    {
        StringBuilder sb = new();
        // column header
        sb.Append('\t');
        sb.AppendLine(string.Join('\t', lightShipCounts.Select(x => x.ToString())));

        for (int rowIndex = 0; rowIndex < matrix.GetLength(0); rowIndex++)
        {
            // row header
            sb.Append(biremeCounts[rowIndex]);
            sb.Append('\t');

            // row content
            for (int colIndex = 0; colIndex < matrix.GetLength(1); colIndex++)
            {
                sb.Append(matrix[rowIndex, colIndex]);
                sb.Append('\t');
            }

            sb.AppendLine();
        }

        Clipboard.SetText(sb.ToString());
    }

    private static int CalculateHowManyLightShipAttacksNeeded(
        int lightShipCountPerAttack,
        int totalBiremeCount)
    {
        int remainingBiremes = totalBiremeCount;
        int attackCount = 0;
        while (remainingBiremes > 0)
        {
            remainingBiremes -= CalculateBiremeLossOnLightShipAttack(lightShipCountPerAttack, remainingBiremes);
            attackCount++;
        }

        return attackCount;
    }

    private static int CalculateBiremeLossOnLightShipAttack(int lightShipCount, int biremeCount)
    {
        int numberOfAttackers = lightShipCount;
        int attackerUnitAttackValue = 200;

        int numberOfDefenders = biremeCount;
        int defendingUnitDefenseValue = 160;

        int totalAttackPower = numberOfAttackers * attackerUnitAttackValue;
        int totalDefensePower = numberOfDefenders * defendingUnitDefenseValue;
        double totalAttackDefenseRatio = totalAttackPower / (double)totalDefensePower;

        const double magicFactor = 0.2;

        double attackEfficiencyFactor = Math.Min(1, Math.Pow(totalAttackDefenseRatio, magicFactor));
        double defenseLossCount = attackEfficiencyFactor * totalAttackPower / defendingUnitDefenseValue;
        int defenseLossRounded = (int)Math.Round(defenseLossCount);
        return defenseLossRounded;
    }
}
