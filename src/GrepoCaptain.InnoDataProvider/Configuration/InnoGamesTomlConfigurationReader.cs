using System;
using System.IO;
using Tomlyn;
using Tomlyn.Model;
using Tomlyn.Syntax;

namespace GrepoCaptain.InnoDataProvider.Configuration;

public sealed class InnoGamesTomlConfigurationReader
{
    private const string ConfigurationFilePath = "config.toml";

    public InnoConfiguration ReadConfiguration()
    {
        string fileContent = File.ReadAllText(ConfigurationFilePath);
        return ParseConfiguration(fileContent);
    }

    private static InnoConfiguration ParseConfiguration(string fileContent)
    {
        DocumentSyntax docSyntax = Toml.Parse(fileContent);
        if (docSyntax.HasErrors)
            throw new Exception("TOML syntax is invalid");

        TomlTable table = docSyntax.ToModel();

        string prefix = (string)table["innodata_prefix"];
        string player = (string)table["innodata_player"];
        string alliance = (string)table["innodata_alliance"];
        string town = (string)table["innodata_town"];
        string island = (string)table["innodata_island"];
        string playerAll = (string)table["innodata_player_all"];
        string playerAtt = (string)table["innodata_player_att"];
        string playerDef = (string)table["innodata_player_def"];
        string allianceAll = (string)table["innodata_alliance_all"];
        string allianceAtt = (string)table["innodata_alliance_att"];
        string allianceDef = (string)table["innodata_alliance_def"];
        string conquers = (string)table["innodata_conquers"];
        string units = (string)table["innodata_static_units"];
        string researches = (string)table["innodata_static_researches"];
        string buildings = (string)table["innodata_static_buildings"];

        return new InnoConfiguration(
            prefix, player, alliance, town, island,
            playerAll, playerAtt, playerDef,
            allianceAll, allianceAtt, allianceDef,
            conquers, units, researches, buildings);
    }
}
