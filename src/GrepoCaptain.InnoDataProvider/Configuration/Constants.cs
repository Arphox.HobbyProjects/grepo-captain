namespace GrepoCaptain.InnoDataProvider.Configuration;

public static class Constants
{
    public static class HungarianWorldIdentifiers
    {
        public const string Carphi = "hu85";
        public const string Bhrytosz = "hu84";
        public const string Abdéra = "hu83";
        public const string Zancle = "hu82";
        public const string Tarasz = "hu81";
        public const string Szinub = "hu80";
        public const string Edessa = "hu50";
        public const string Hyperborea = "hu16";
        public const string Achilles = "hu13";
    }
}
