using System.Net;

namespace GrepoCaptain.InnoDataProvider.Utils;

internal static class StringUtils
{
    internal static string DecodeString(string str)
    {
        return WebUtility.UrlDecode(str);
    }
}
