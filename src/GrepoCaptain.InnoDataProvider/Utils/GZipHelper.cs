using System.IO;
using System.IO.Compression;
using System.Text;

namespace GrepoCaptain.InnoDataProvider.Utils;

internal static class GZipHelper
{
    internal static string Decompress(Stream stream)
    {
        using MemoryStream outputStream = new();
        using (GZipStream gs = new(stream, CompressionMode.Decompress))
            CopyStream(gs, outputStream);

        return Encoding.UTF8.GetString(outputStream.ToArray());
    }

    private static void CopyStream(Stream source, Stream destination, int bufferSize = 65_536)
    {
        byte[] bytes = new byte[bufferSize];
        int counter;
        while ((counter = source.Read(bytes, 0, bytes.Length)) != 0)
            destination.Write(bytes, 0, counter);
    }
}
