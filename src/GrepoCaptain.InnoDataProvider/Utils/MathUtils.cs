﻿using System;

namespace GrepoCaptain.InnoDataProvider.Utils;

public class MathUtils
{
    public static double DistanceTo(int x1, int y1, int x2, int y2)
    {
        double distance = Math.Sqrt(Math.Pow(x1 - x2, 2) + Math.Pow(y1 - y2, 2));
        return Math.Round(distance, 2);
    }
}
