using System.Linq;
using System.Threading.Tasks;
using GrepoCaptain.InnoDataProvider.Model;
using GrepoCaptain.InnoDataProvider.Model.Json.Buildings;
using GrepoCaptain.InnoDataProvider.Model.Json.Units;
using Newtonsoft.Json.Linq;

namespace GrepoCaptain.InnoDataProvider.DataProvider;

public sealed class InnoDataSnapshotProvider
{
    private readonly InnoDataSnapshotDownloader _innoDataSnapshotDownloader;

    public InnoDataSnapshotProvider(InnoDataSnapshotDownloader innoDataSnapshotDownloader)
    {
        _innoDataSnapshotDownloader = innoDataSnapshotDownloader;
    }

    public async Task<Player[]> FetchPlayers()
    {
        return (await _innoDataSnapshotDownloader.DownloadPlayers())
            .Select(Player.ParsePlayer)
            .ToArray();
    }

    public async Task<Town[]> FetchTowns()
    {
        return (await _innoDataSnapshotDownloader.DownloadTowns())
            .Select(Town.ParseTown)
            .ToArray();
    }

    public async Task<Island[]> FetchIslands()
    {
        return (await _innoDataSnapshotDownloader.DownloadIslands())
            .Select(Island.ParseIsland)
            .Where(i => !i.IsDecorativeRock)
            .ToArray();
    }

    public async Task<Alliance[]> FetchAlliances()
    {
        return (await _innoDataSnapshotDownloader.DownloadAlliances())
            .Select(Alliance.ParseAlliance)
            .ToArray();
    }

    public async Task<Conquer[]> FetchConquers()
    {
        return (await _innoDataSnapshotDownloader.DownloadConquers())
            .Select(Conquer.ParseConquer)
            .ToArray();
    }

    public async Task<UnitData[]> FetchStaticUnits()
    {
        string json = await _innoDataSnapshotDownloader.DownloadStaticUnits();

        JObject rootObject = JObject.Parse(json);
        UnitData[] units = rootObject
            .Children<JProperty>()
            .Select(ch => ch.Value.ToObject<UnitData>())
            .ToArray();

        return units;
    }

    public async Task<BuildingData[]> FetchStaticBuildings()
    {
        string json = await _innoDataSnapshotDownloader.DownloadStaticBuildings();

        JObject rootObject = JObject.Parse(json);
        BuildingData[] buildings = rootObject
            .Children<JProperty>()
            .Select(ch => ch.Value.ToObject<BuildingData>())
            .ToArray();

        return buildings;
    }
}
