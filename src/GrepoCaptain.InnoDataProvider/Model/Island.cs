using System;
using GrepoCaptain.Common.Game;
using GrepoCaptain.InnoDataProvider.Model.Json.Enums;

namespace GrepoCaptain.InnoDataProvider.Model;

public record Island : IHasBbCode
{
    /// <summary>
    /// Unique id of the island, not really useful for anything
    /// other than linking the island in-game with BB code.
    /// </summary>
    public int Id { get; }

    /// <summary>
    /// The X coordinate (horizontal) of the island's top left(?) corner.
    /// </summary>
    /// <remarks>Increases from left to right.</remarks>
    public int X { get; }

    /// <summary>
    /// The Y coordinate (vertical) of the island's top left(?) corner.
    /// </summary>
    /// <remarks>Increases from top to bottom.</remarks>
    public int Y { get; }

    /// <summary>
    /// The numeric Type value of the island, which describes
    /// what kind of island is it: inhabited / uninhabited / rock(decoration)
    /// </summary>
    public int NumericType { get; }

    /// <summary>
    /// Tells how many available town slots are on the island.
    /// </summary>
    public int AvailableTownSlots { get; }

    /// <summary>
    /// Tells what kind of resource do towns on the island produce more.
    /// </summary>
    public ResourceType MoreResource { get; }

    /// <summary>
    /// Tells what kind of resource do towns on the island produce less.
    /// </summary>
    public ResourceType LessResource { get; }


    /// <summary>
    /// A unique island identifier which is derived from island coordinates.
    /// </summary>
    /// <remarks>
    /// It is useful because town data only contain IslandX and IslandY and
    /// not the island id, so this way islands can be identified from town data
    /// only.
    /// </remarks>
    public int IslandDesignator { get; }

    /// <summary>
    /// Returns true if there are farm villages are present on the island,
    /// otherwise false.
    /// </summary>
    public bool IsInhabited { get; }

    /// <summary>
    /// The total number of town slots, so the maximum town capacity of the island.
    /// </summary>
    public int TotalTownSlots { get; }

    /// <summary>
    /// The ocean number which tells what ocean the island is in.
    /// </summary>
    public int OceanNumber { get; }

    /// <summary>
    /// Calculates the number of taken town slots.
    /// </summary>
    public int TakenTownSlotCount => TotalTownSlots - AvailableTownSlots;

    /// <summary>
    /// Calculates whether the island is considered a rock;
    /// that is an empty island where towns cannot exist.
    /// These are only for decoration, but for some reason
    /// they are include in data sets.
    /// </summary>
    public bool IsDecorativeRock => NumericType is >= 17 and <= 36;

    private Island(
        int id,
        int x,
        int y,
        int numericType,
        int availableTownSlots,
        string moreResource,
        string lessResource)
    {
        Id = id;
        X = x;
        Y = y;
        NumericType = numericType;
        AvailableTownSlots = availableTownSlots;
        MoreResource = ResourceTypeParser.Parse(moreResource);
        LessResource = ResourceTypeParser.Parse(lessResource);

        OceanNumber = CalculateOceanNumber(x, y);
        IslandDesignator = CalculateDesignator(x, y);
        IsInhabited = CalculateIsInhabited(numericType);
        TotalTownSlots = GetTotalTownSlots(numericType);
    }

    public string ToBbCode()
    {
        return $"[island]{Id}[/island]";
    }

    /// <summary>
    /// Calculates the ocean number of an island.
    /// </summary>
    /// <param name="x">The X coordinate of the island.</param>
    /// <param name="y">The Y coordinate of the island.</param>
    /// <remarks>
    /// Formula found out by heuristics, is not guaranteed to be correct.
    /// </remarks>
    private static int CalculateOceanNumber(int x, int y)
    {
        int tenFoldValue = x / 100;
        int oneFoldValue = y / 100;
        return tenFoldValue * 10 + oneFoldValue;
    }

    /// <summary>
    /// Calculates a designator number from coordinates which can serve
    /// as a meaningful secondary island identifier.<br/>
    /// </summary>
    /// <remarks>
    /// value = x * 10000 + y<br/>
    /// This is valid because the largest island x or y coordinate is 1000.
    /// </remarks>
    internal static int CalculateDesignator(int x, int y)
    {
        return x * 10000 + y;
    }

    /// <remarks>
    /// 1..10: island1.png ... island10.png
    /// 11..16: uninhabited1.png ... uninhabited6.png
    /// 17..36: rock1.png ... rock20.png
    /// 37..46: island11.png ... island20.png
    /// 47..60: uninhabited7.png ... uninhabited20.png
    /// Source: https://en.forum.grepolis.com/index.php?threads/collection-of-useful-grepo-algorithms.52048/
    /// </remarks>
    private static bool CalculateIsInhabited(int type)
    {
        return type switch
        {
            <= 10 => true,
            <= 16 => false, // uninhabited
            <= 36 => false, // rock, 0 town slots
            <= 46 => true,
            <= 60 => false, // uninhabited
            _ => throw new ArgumentOutOfRangeException(nameof(type), type, "Unexpected island type"),
        };
    }

    /// <remarks>
    /// Calculated by looking at data from hu69 server, and making statistics of the
    /// available town count for a given type.
    /// </remarks>
    private static int GetTotalTownSlots(int type)
    {
        return type switch
        {
            <= 10 => 20, // inhabited
            <= 12 => 7,
            13 => 11,
            14 => 7,
            15 => 8,
            16 => 13,
            <= 36 => 0, // rocks
            <= 46 => 20, // inhabited
            47 => 9,
            48 => 8,
            49 => 9,
            50 => 10,
            51 => 8,
            52 => 10,
            53 => 11,
            54 => 10,
            55 => 9,
            56 => 10,
            57 => 8,
            58 => 6,
            59 => 8,
            60 => 9,
            _ => throw new ArgumentOutOfRangeException(nameof(type), type, "Unexpected island type"),
        };
    }

    internal static Island ParseIsland(string row)
    {
        // Example row:
        // 60009,460,509,8,0,stone,wood

        string[] parts = row.Split(',');

        int id = int.Parse(parts[0]);
        int x = int.Parse(parts[1]);
        int y = int.Parse(parts[2]);
        int type = int.Parse(parts[3]);
        int numberOfAvailableTowns = int.Parse(parts[4]);
        string moreResource = parts[5];
        string lessResource = parts[6];

        return new Island(id, x, y, type, numberOfAvailableTowns, moreResource, lessResource);
    }
}
