namespace GrepoCaptain.InnoDataProvider.Model.Json.Buildings;

public record BuildTimeFactor
{
    public int Level { get; init; }
    public double Factor { get; init; }
}
