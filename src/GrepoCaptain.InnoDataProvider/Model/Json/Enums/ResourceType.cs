using System;

namespace GrepoCaptain.InnoDataProvider.Model.Json.Enums;

public enum ResourceType
{
    Wood = 1,
    Stone = 2,
    Iron = 3,
}

internal static class ResourceTypeParser
{
    internal static ResourceType Parse(string resource)
    {
        return resource switch
        {
            "wood" => ResourceType.Wood,
            "stone" => ResourceType.Stone,
            "iron" => ResourceType.Iron,
            _ => throw new ArgumentOutOfRangeException(nameof(resource), resource, "Unexpected resource type"),
        };
    }
}
