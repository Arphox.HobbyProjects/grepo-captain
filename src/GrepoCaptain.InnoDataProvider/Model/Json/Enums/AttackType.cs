namespace GrepoCaptain.InnoDataProvider.Model.Json.Enums;

public enum AttackType
{
    Naval = 0,
    Hack = 1,
    Pierce = 2,
    Distance = 3,
}
