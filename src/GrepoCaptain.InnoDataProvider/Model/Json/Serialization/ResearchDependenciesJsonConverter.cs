using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace GrepoCaptain.InnoDataProvider.Model.Json.Serialization;

public class ResearchDependenciesJsonConverter : JsonConverter
{
    public override bool CanWrite => false;

    public override object ReadJson(
        JsonReader reader,
        Type objectType,
        object existingValue,
        JsonSerializer serializer)
    {
        if (reader.TokenType == JsonToken.Null)
            return Array.Empty<string>();

        return JArray.Load(reader).ToObject<string[]>();
    }

    public override bool CanConvert(Type objectType)
    {
        throw new NotImplementedException();
    }

    public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
    {
        throw new NotImplementedException();
    }
}
