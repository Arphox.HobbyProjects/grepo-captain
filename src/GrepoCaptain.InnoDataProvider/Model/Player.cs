using GrepoCaptain.Common.Game;
using GrepoCaptain.InnoDataProvider.Utils;

namespace GrepoCaptain.InnoDataProvider.Model;

public record Player : IHasBbCode
{
    public int PlayerId { get; private init; }
    public string Name { get; private init; }
    public int? AllianceId { get; private init; }
    public int Points { get; private init; }
    public int Rank { get; private init; }
    public int NumberOfTowns { get; private init; }

    public string ToBbCode()
    {
        return $"[player]{Name}[/player]";
    }

    internal static Player ParsePlayer(string row)
    {
        // Example row:
        // 872197,Arphox,40,1109003,5,66

        string[] parts = row.Split(',');

        int playerId = int.Parse(parts[0]);
        string name = StringUtils.DecodeString(parts[1]);
        int? allianceId = ParseHelper.ParseNullableInt(parts[2]);
        int points = int.Parse(parts[3]);
        int rank = int.Parse(parts[4]);
        int numberOfTowns = int.Parse(parts[5]);

        return new Player
        {
            PlayerId = playerId,
            Name = name,
            AllianceId = allianceId,
            Points = points,
            Rank = rank,
            NumberOfTowns = numberOfTowns,
        };
    }
}
