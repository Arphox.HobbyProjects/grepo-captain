using GrepoCaptain.Common.Game;
using GrepoCaptain.InnoDataProvider.Utils;

namespace GrepoCaptain.InnoDataProvider.Model;

public record Town : IHasBbCode
{
    public int TownId { get; }
    public int? PlayerId { get; }
    public string Name { get; }

    /// <summary>
    /// The X coordinate of the island the town is on.
    /// </summary>
    public int IslandX { get; }

    /// <summary>
    /// The Y coordinate of the island the town is on.
    /// </summary>
    public int IslandY { get; }

    /// <summary>
    /// Tells on the island which slot the town is at.
    /// The slot number is indexed from 0 and increases clockwise around the island
    /// (derived from heuristics).
    /// </summary>
    public int SlotOnIsland { get; }

    /// <summary>
    /// Tells how many points the town has.
    /// </summary>
    public int Points { get; }

    /// <inheritdoc cref="Island.IslandDesignator"/>
    public int IslandDesignator { get; }

    public bool IsGhostTown => PlayerId == null;

    private Town(
        int townId,
        int? playerId,
        string name,
        int islandX,
        int islandY,
        int slotOnIsland,
        int points)
    {
        TownId = townId;
        PlayerId = playerId;
        Name = name;
        IslandX = islandX;
        IslandY = islandY;
        SlotOnIsland = slotOnIsland;
        Points = points;

        IslandDesignator = Island.CalculateDesignator(islandX, islandY);
    }

    public string ToBbCode()
    {
        return $"[town]{TownId}[/town]";
    }

    /// <summary>
    /// Returns an distance between this town's island and
    /// the <paramref name="other"/> town's island.
    /// </summary>
    /// <remarks>
    /// We don't get exact coordinates from InnoGames so this is useful
    /// for an estimate.
    /// </remarks>
    public double IslandDistanceTo(Town other)
    {
        return MathUtils.DistanceTo(IslandX, IslandY, other.IslandX, other.IslandY);
    }

    /// <summary>
    /// Returns an distance between this town's island and
    /// the <paramref name="other"/> island.
    /// </summary>
    public double IslandDistanceTo(Island other)
    {
        return MathUtils.DistanceTo(IslandX, IslandY, other.X, other.Y);
    }

    internal static Town ParseTown(string row)
    {
        // Example row:
        // 3431,872197,ARP+37,489,445,10,17786

        string[] parts = row.Split(',');

        int townId = int.Parse(parts[0]);
        int? playerId = ParseHelper.ParseNullableInt(parts[1]);
        string name = StringUtils.DecodeString(parts[2]);
        int islandX = int.Parse(parts[3]);
        int islandY = int.Parse(parts[4]);
        int numberOnIsland = int.Parse(parts[5]);
        int points = int.Parse(parts[6]);

        return new Town(townId, playerId, name, islandX, islandY, numberOnIsland, points);
    }
}
